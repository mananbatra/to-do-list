import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  
  const [listItems, setListItem]=useState([])

  const [input, setInput]=useState('')

  function inputHandle(event)
  {
    setInput(event.target.value)
  }
  function handleClick()
  {
      
      setListItem((listItems)=>[...listItems,input])
  }
  return(
    <>
      <input name="textInput" type='text' placeholder='Enter items for To-Do list' onChange={inputHandle}></input>
      <button onClick={handleClick}>Add to TO-Do List</button>
      <ul>
        {listItems.map((item,index)=>
        {
          return <li id={index}>{item}</li>
        })}
      </ul>
    </>
  )
}

export default App
